bg = new BackgroundLayer
	backgroundColor: "rgba(243, 245, 232, 1)"
	
bgScrollingMapContainer = new Layer
	width: Screen.width
	height: Screen.height
	backgroundColor: "null"

signUpContainer = new Layer
	backgroundColor: "rgba(199, 217, 187, 1)"
	midX: bg.midX

if Screen.width < 680
	signUpContainer.width = Screen.width * 8 / 10
else
	signUpContainer.width = 640

signUpContainer.center()


spacingBetweenFields = 20
fieldWidth = signUpContainer.width * 3/4
fieldHeight = 42

signUpContainer__userNameBox = new Layer
	superLayer: signUpContainer
	height: fieldHeight
	width: fieldWidth
	backgroundColor: "#fff"
	y: 3*spacingBetweenFields

signUpContainer__userNameBox.centerX()

signUpContainer__userMailBox = new Layer
	superLayer: signUpContainer
	height: fieldHeight
	width: fieldWidth
	backgroundColor: "#fff"
	y: signUpContainer__userNameBox.height + signUpContainer__userNameBox.y + spacingBetweenFields

signUpContainer__userMailBox.centerX()

signUpContainer__userPasswordBox = new Layer
	superLayer: signUpContainer
	height: fieldHeight
	width: fieldWidth
	backgroundColor: "#fff"
	y: signUpContainer__userMailBox.height + signUpContainer__userMailBox.y + spacingBetweenFields

signUpContainer__userPasswordBox.centerX()

signUpContainer__signUpBtn = new Layer
	superLayer: signUpContainer
	backgroundColor: "rgba(186, 145, 151, 1)"
	height: fieldHeight
	width: fieldWidth
	y: signUpContainer__userPasswordBox.height + signUpContainer__userPasswordBox.y + spacingBetweenFields

signUpContainer__signUpBtn.centerX()

signUpContainer.height = signUpContainer.contentFrame().height + 6*spacingBetweenFields

